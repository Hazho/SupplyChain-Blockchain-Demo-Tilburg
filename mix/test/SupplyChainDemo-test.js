// file header
/**
* A simple bean class around the contract.
* The QuotationModel.
**/
function QuotationModel(contract) {
this.contract = contract;
	/**
	* Getter for suppliermanagement.
	**/
	this.getSuppliermanagement = function(){
		return contract.suppliermanagement(); 
	}
	/**
	* Getter for TransactionID.
	**/
	this.getTransactionID = function(){
		return contract.TransactionID(); 
	}
	/**
	* Getter for IssueDate.
	**/
	this.getIssueDate = function(){
		return contract.IssueDate(); 
	}
	/**
	* Getter for FreightTerms.
	**/
	this.getFreightTerms = function(){
		return contract.FreightTerms(); 
	}
	/**
	* Getter for PaymentTerms.
	**/
	this.getPaymentTerms = function(){
		return contract.PaymentTerms(); 
	}
	/**
	* Call AddNewQuotation.
	**/
	this.AddNewQuotation = function(){
		return contract.AddNewQuotation(); 
	}
}// end of function QuotationModel

//test class for Quotation
function TestQuotation(contract) {
	
	this.test_instance = contract;
	this.model = new QuotationModel(contract);
	this.prefix='';
	this.messageBlockId = "testResult";
	var self = this;

	this.testSetup=function(){
		//Start of user code testSetup_Quotation
		//TODO: implement
		//End of user code
	}

	this.allTests=function(){
		this.testSetup();
		this.testAttributes();
		this.testQuotation_AddNewQuotation();
		this.customTests();
	
		//Start of user code allTests_Quotation
		//TODO: implement
		//End of user code

	}
	
	//print the test result
	this.printTest=function(testName,testMessage,state){
		var e = document.getElementById(this.prefix+'-'+this.messageBlockId);
		var elemDiv = document.createElement('div');
		elemDiv.id= this.prefix+'-'+testName;
		elemDiv.className='testRow';
		elemDiv.text=testMessage;
		var stateDiv = document.createElement('div');
		if(state){
			elemDiv.innerHTML = '<div class="pass_state">P</div><div class="testCell">'+testMessage+'</div>';
		}else{
			elemDiv.innerHTML = '<div class="failed_state">F</div><div class="testCell">'+testMessage+'</div>';
		}
		e.appendChild(elemDiv);
	}

	//assertEquals
	this.testAE=function(testName,testMessage,expected,value) {
		if(expected==value)
			this.printTest(testName, testMessage, true);
		else
			this.printTest(testName, testMessage+': expected '+expected+' got '+value, false);
	}

	//test the attributes after setup	
	this.testAttributes=function() {
	//Start of user code attributeTests_Quotation
	//TODO: implement
	//End of user code
	}

	//Test for Quotation_AddNewQuotation
	this.testQuotation_AddNewQuotation=function() {
		//	var res = this.test_instance.AddNewQuotation();
		//Start of user code test_Quotation_AddNewQuotation
		//TODO : implement this
		//var test = false;		
		//this.testAE("testAddNewQuotation", "executed: testQuotation_AddNewQuotation",true, test);		
		//End of user code
	}
	this.customTests=function() {
		//Start of user code test_Quotation_custom tests
		//
		//End of user code
	}
}
/**
* A simple bean class around the contract.
* The ShippingMethodModel.
**/
function ShippingMethodModel(contract) {
this.contract = contract;
	/**
	* Getter for warehouse.
	**/
	this.getWarehouse = function(){
		return contract.warehouse(); 
	}
}// end of function ShippingMethodModel

//test class for ShippingMethod
function TestShippingMethod(contract) {
	
	this.test_instance = contract;
	this.model = new ShippingMethodModel(contract);
	this.prefix='';
	this.messageBlockId = "testResult";
	var self = this;

	this.testSetup=function(){
		//Start of user code testSetup_ShippingMethod
		//TODO: implement
		//End of user code
	}

	this.allTests=function(){
		this.testSetup();
		this.testAttributes();
		this.customTests();
	
		//Start of user code allTests_ShippingMethod
		//TODO: implement
		//End of user code

	}
	
	//print the test result
	this.printTest=function(testName,testMessage,state){
		var e = document.getElementById(this.prefix+'-'+this.messageBlockId);
		var elemDiv = document.createElement('div');
		elemDiv.id= this.prefix+'-'+testName;
		elemDiv.className='testRow';
		elemDiv.text=testMessage;
		var stateDiv = document.createElement('div');
		if(state){
			elemDiv.innerHTML = '<div class="pass_state">P</div><div class="testCell">'+testMessage+'</div>';
		}else{
			elemDiv.innerHTML = '<div class="failed_state">F</div><div class="testCell">'+testMessage+'</div>';
		}
		e.appendChild(elemDiv);
	}

	//assertEquals
	this.testAE=function(testName,testMessage,expected,value) {
		if(expected==value)
			this.printTest(testName, testMessage, true);
		else
			this.printTest(testName, testMessage+': expected '+expected+' got '+value, false);
	}

	//test the attributes after setup	
	this.testAttributes=function() {
	//Start of user code attributeTests_ShippingMethod
	//TODO: implement
	//End of user code
	}
	this.customTests=function() {
		//Start of user code test_ShippingMethod_custom tests
		//
		//End of user code
	}
}
/**
* A simple bean class around the contract.
* The ContractModel.
**/
function ContractModel(contract) {
this.contract = contract;
	/**
	* Getter for purchaseorder.
	**/
	this.getPurchaseorder = function(){
		return contract.purchaseorder(); 
	}
}// end of function ContractModel

//test class for Contract
function TestContract(contract) {
	
	this.test_instance = contract;
	this.model = new ContractModel(contract);
	this.prefix='';
	this.messageBlockId = "testResult";
	var self = this;

	this.testSetup=function(){
		//Start of user code testSetup_Contract
		//TODO: implement
		//End of user code
	}

	this.allTests=function(){
		this.testSetup();
		this.testAttributes();
		this.customTests();
	
		//Start of user code allTests_Contract
		//TODO: implement
		//End of user code

	}
	
	//print the test result
	this.printTest=function(testName,testMessage,state){
		var e = document.getElementById(this.prefix+'-'+this.messageBlockId);
		var elemDiv = document.createElement('div');
		elemDiv.id= this.prefix+'-'+testName;
		elemDiv.className='testRow';
		elemDiv.text=testMessage;
		var stateDiv = document.createElement('div');
		if(state){
			elemDiv.innerHTML = '<div class="pass_state">P</div><div class="testCell">'+testMessage+'</div>';
		}else{
			elemDiv.innerHTML = '<div class="failed_state">F</div><div class="testCell">'+testMessage+'</div>';
		}
		e.appendChild(elemDiv);
	}

	//assertEquals
	this.testAE=function(testName,testMessage,expected,value) {
		if(expected==value)
			this.printTest(testName, testMessage, true);
		else
			this.printTest(testName, testMessage+': expected '+expected+' got '+value, false);
	}

	//test the attributes after setup	
	this.testAttributes=function() {
	//Start of user code attributeTests_Contract
	//TODO: implement
	//End of user code
	}
	this.customTests=function() {
		//Start of user code test_Contract_custom tests
		//
		//End of user code
	}
}
