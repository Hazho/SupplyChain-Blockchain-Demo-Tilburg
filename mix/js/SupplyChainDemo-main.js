// file header
// contractVariable for Quotation
var QuotationContract = web3.eth.contract([
{"constant":true,"inputs":[],"name":"suppliermanagement","outputs":[{"name":"","type":"SupplierManagement"}],"type":"function"},
{"constant":true,"inputs":[],"name":"TransactionID","outputs":[{"name":"","type":""}],"type":"function"},
{"constant":true,"inputs":[],"name":"IssueDate","outputs":[{"name":"","type":""}],"type":"function"},
{"constant":true,"inputs":[],"name":"FreightTerms","outputs":[{"name":"","type":""}],"type":"function"},
{"constant":true,"inputs":[],"name":"PaymentTerms","outputs":[{"name":"","type":""}],"type":"function"},
{ "constant": false,
    "inputs": [],    
    "name": "AddNewQuotation",
    "outputs": [],
    "type": "function" }
,
  { "anonymous": false,
    "inputs": 
	[
	],    
    "name": "GetPrice",
    "type": "event"  }
]);   
// contractVariable for ShippingMethod
var ShippingMethodContract = web3.eth.contract([
{"constant":true,"inputs":[],"name":"warehouse","outputs":[{"name":"","type":"Warehouse"}],"type":"function"},

]);   
// contractVariable for Contract
var ContractContract = web3.eth.contract([
{"constant":true,"inputs":[],"name":"purchaseorder","outputs":[{"name":"","type":"PurchaseOrder"}],"type":"function"},

]);   


/**
* A simple bean class around the contract.
* The QuotationModel.
**/
function QuotationModel(contract) {
this.contract = contract;
	/**
	* Getter for suppliermanagement.
	**/
	this.getSuppliermanagement = function(){
		return contract.suppliermanagement(); 
	}
	/**
	* Getter for TransactionID.
	**/
	this.getTransactionID = function(){
		return contract.TransactionID(); 
	}
	/**
	* Getter for IssueDate.
	**/
	this.getIssueDate = function(){
		return contract.IssueDate(); 
	}
	/**
	* Getter for FreightTerms.
	**/
	this.getFreightTerms = function(){
		return contract.FreightTerms(); 
	}
	/**
	* Getter for PaymentTerms.
	**/
	this.getPaymentTerms = function(){
		return contract.PaymentTerms(); 
	}
	/**
	* Call AddNewQuotation.
	**/
	this.AddNewQuotation = function(){
		return contract.AddNewQuotation(); 
	}
}// end of function QuotationModel

/**
* Gui factory Quotation
**/
function QuotationGuiFactory() {
	this.prefix='';
	
	/**
	* Places the default gui to 'e' or an element with id='Quotation_gui'
	*/
	this.placeDefaultGui=function(e) {
		if(e==null)
			e = document.getElementById(this.prefix+'Quotation_gui');
		if(e!=null)
			e.innerHTML = this.createDefaultGui();
		else
			console.log(this.prefix+'Quotation_gui not found');
	}

	/**
	* The default generated gui with all actions and attributes.
	*/
	this.createDefaultGui=function() {
		return 		'<!-- gui for Quotation_contract -->'
+		'		<div class="contract" id="'+this.prefix+'Quotation_contract">'
+		'		Quotation:'
+		'		  <input type="text" id="'+this.prefix+'Quotation_address"> <button id="'+this.prefix+'QuotationController.setAddress" onclick="'+this.prefix+'QuotationController.setAddress()">change Quotation Address</button>'
+		'		  <div class="contract_attributes" id="'+this.prefix+'Quotation_contract_attributes"> attributes:'
+		'		    <div class="contract_attribute" id="'+this.prefix+'Quotation_contract_attribute_suppliermanagement"> suppliermanagement:'
+		'		      <span class="contract_attribute_value" id="'+this.prefix+'Quotation_suppliermanagement_value"> </span>'
+		'		    </div>'
+		'		    <div class="contract_attribute" id="'+this.prefix+'Quotation_contract_attribute_TransactionID"> TransactionID:'
+		'		      <span class="contract_attribute_value" id="'+this.prefix+'Quotation_TransactionID_value"> </span>'
+		'		    </div>'
+		'		    <div class="contract_attribute" id="'+this.prefix+'Quotation_contract_attribute_IssueDate"> IssueDate:'
+		'		      <span class="contract_attribute_value" id="'+this.prefix+'Quotation_IssueDate_value"> </span>'
+		'		    </div>'
+		'		    <div class="contract_attribute" id="'+this.prefix+'Quotation_contract_attribute_FreightTerms"> FreightTerms:'
+		'		      <span class="contract_attribute_value" id="'+this.prefix+'Quotation_FreightTerms_value"> </span>'
+		'		    </div>'
+		'		    <div class="contract_attribute" id="'+this.prefix+'Quotation_contract_attribute_PaymentTerms"> PaymentTerms:'
+		'		      <span class="contract_attribute_value" id="'+this.prefix+'Quotation_PaymentTerms_value"> </span>'
+		'		    </div>'
+		'		'
+		'		    <button class="function_btn" id="'+this.prefix+'Quotation_updateAttributes" onclick="'+this.prefix+'QuotationController._updateAttributes()">update Quotation attributes</button>'
+		'		  </div>'
+		'		  <fieldset class="function_execution" id="'+this.prefix+'Quotation_contract_function_Quotation_AddNewQuotation">'
+		'		<legend>AddNewQuotation</legend>'
+		'			<button class="function_btn" id="'+this.prefix+'QuotationController.Quotation_AddNewQuotation" onclick="'+this.prefix+'QuotationController.Quotation_AddNewQuotation()">AddNewQuotation</button>'
+		'			<div class="function_result" id="'+this.prefix+'Quotation_AddNewQuotation_res"></div>'
+		'		  </fieldset>'
+		'		'
+		'		</div>'
;
	}

	/**
	* Create the gui for the attributes.
	*/
	this.createAttributesGui=function() {
		return 		'    <div class="contract_attribute" id="'+this.prefix+'Quotation_contract_attribute_suppliermanagement"> suppliermanagement:'
+		'		      <span class="contract_attribute_value" id="'+this.prefix+'Quotation_suppliermanagement_value"> </span>'
+		'		    </div>'
+		'		    <div class="contract_attribute" id="'+this.prefix+'Quotation_contract_attribute_TransactionID"> TransactionID:'
+		'		      <span class="contract_attribute_value" id="'+this.prefix+'Quotation_TransactionID_value"> </span>'
+		'		    </div>'
+		'		    <div class="contract_attribute" id="'+this.prefix+'Quotation_contract_attribute_IssueDate"> IssueDate:'
+		'		      <span class="contract_attribute_value" id="'+this.prefix+'Quotation_IssueDate_value"> </span>'
+		'		    </div>'
+		'		    <div class="contract_attribute" id="'+this.prefix+'Quotation_contract_attribute_FreightTerms"> FreightTerms:'
+		'		      <span class="contract_attribute_value" id="'+this.prefix+'Quotation_FreightTerms_value"> </span>'
+		'		    </div>'
+		'		    <div class="contract_attribute" id="'+this.prefix+'Quotation_contract_attribute_PaymentTerms"> PaymentTerms:'
+		'		      <span class="contract_attribute_value" id="'+this.prefix+'Quotation_PaymentTerms_value"> </span>'
+		'		    </div>'
+		'		'
;
	}

	/**
	* Create the gui.
	*/
	this.createPlainGui=function(){
		return this.createAttributesGui()
				+ this.createQuotation_AddNewQuotationGui
				;
	}

	/**
	* Create the gui for the function AddNewQuotation.
	*/
	this.createQuotation_AddNewQuotationGui=function() {
		return 		'  <fieldset class="function_execution" id="'+this.prefix+'Quotation_contract_function_Quotation_AddNewQuotation">'
+		'		<legend>AddNewQuotation</legend>'
+		'			<button class="function_btn" id="'+this.prefix+'QuotationController.Quotation_AddNewQuotation" onclick="'+this.prefix+'QuotationController.Quotation_AddNewQuotation()">AddNewQuotation</button>'
+		'			<div class="function_result" id="'+this.prefix+'Quotation_AddNewQuotation_res"></div>'
+		'		  </fieldset>'
;
	}

	/**
	* Create a div with '@inner' as inner elements.
    * @inner - the inner text
	*/
	this.createSeletonGui=function(inner) {
		return 	'<!-- gui for Quotation_contract -->'
		+	'	<div class="contract" id="'+this.prefix+'Quotation_contract">'
		+ inner
		+'</div>';
	}


	//eventguis

	/**
	* Create a gui for the GetPrice event.
    * @prefix - a prefix
	* @blockHash - the bolckhash 
	* @blockNumber - the number of the block
	*/
	this.createGetPriceLogDataGui = function(prefix, blockHash, blockNumber
	) {
		return '<div class="eventRow">'
        +' </div>';
	}

}//end guifactory

/**
* Class QuotationController. 
* The controller wrap's the 'instance' contract and binds all actions to document elements.
* Parameters are taken from elements with self.prefix+'functionName_parameterName'
*
* self.prefix+'QuotationController.setAddress' - 
* self.prefix+'Quotation_updateAttributes'     - 
* self.prefix+'Quotation_AddNewQuotation -
*/
function QuotationController() {

	this.instance = undefined;
	this.prefix='';
	this.contractAddress = undefined; 
	this.eventlogPrefix = '';
	var self = this;

	/**
	* Binds the element with the prefix-id to the corresponding element.
	*/
	this.bindGui=function() {
		console.log('bind gui for:'+self.prefix);
		var btn = document.getElementById(self.prefix+'QuotationController.setAddress');
		if(btn!=undefined)		
			btn.onclick = this.setAddress;
		else console.log('addres widget not bound');

		var btn = document.getElementById(self.prefix+'Quotation_updateAttributes');
		if(btn!=undefined)
			btn.onclick = this._updateAttributes;
		else console.log('_updateAttributes widget not bound');
		var btn = document.getElementById(self.prefix+'QuotationController.Quotation_AddNewQuotation');
		if(btn!=undefined)
			btn.onclick = this.Quotation_AddNewQuotation;
		else console.log('Quotation_AddNewQuotation widget not bound');

	}

	/**
	* Set the address.
	*/
	this.setAddress=function() {
		var _address = document.getElementById(self.prefix+'Quotation_address');
		if(_address==null)return;

		self.Quotation_instance = QuotationContract.at(_address.value);
		self.contractAddress = _address.value;
		self._updateAttributes();
	}
	
	/**
	* Update attributes.
	*
	* prefix+'Quotation_suppliermanagement_value' - 
	* prefix+'Quotation_TransactionID_value' - 
	* prefix+'Quotation_IssueDate_value' - 
	* prefix+'Quotation_FreightTerms_value' - 
	* prefix+'Quotation_PaymentTerms_value' - 
	**/
	this._updateAttributes=function () {
		if(this.instance===null) return;
		// update attributes
		var suppliermanagement_res = self.instance.suppliermanagement();
		var e = document.getElementById(self.prefix+'Quotation_suppliermanagement_value');
		if(suppliermanagement_res!=null && e!=null)
			e.innerText = suppliermanagement_res;
		else console.log(self.prefix+'Quotation_suppliermanagement_value not found');
		var TransactionID_res = self.instance.TransactionID();
		var e = document.getElementById(self.prefix+'Quotation_TransactionID_value');
		if(TransactionID_res!=null && e!=null)
			e.innerText = TransactionID_res;
		else console.log(self.prefix+'Quotation_TransactionID_value not found');
		var IssueDate_res = self.instance.IssueDate();
		var e = document.getElementById(self.prefix+'Quotation_IssueDate_value');
		if(IssueDate_res!=null && e!=null)
			e.innerText = IssueDate_res;
		else console.log(self.prefix+'Quotation_IssueDate_value not found');
		var FreightTerms_res = self.instance.FreightTerms();
		var e = document.getElementById(self.prefix+'Quotation_FreightTerms_value');
		if(FreightTerms_res!=null && e!=null)
			e.innerText = FreightTerms_res;
		else console.log(self.prefix+'Quotation_FreightTerms_value not found');
		var PaymentTerms_res = self.instance.PaymentTerms();
		var e = document.getElementById(self.prefix+'Quotation_PaymentTerms_value');
		if(PaymentTerms_res!=null && e!=null)
			e.innerText = PaymentTerms_res;
		else console.log(self.prefix+'Quotation_PaymentTerms_value not found');
	}

	//call functions
	
	/**
	* Calls the contract function Quotation_AddNewQuotation.
	*
	**/
	this.Quotation_AddNewQuotation=function() {
		var res = self.instance.AddNewQuotation();
	}
	
//delegated calls
}// end controller	

/**
* class as GlueCode QuotationManager,
* uses prefix + 'GuiContainer' as target element.
* It combines a QuotationController as 'c' and a QuotationGuiFactory as 'g'.
**/
function QuotationManager(prefix,contract,containerId) {
	this.prefix = prefix;
	var self = this;
	this.c = new QuotationController();
	this.c.prefix=prefix;
	this.c.instance=contract;
	this.c.contractAddress = contract.address;
	this.g = new QuotationGuiFactory();
	this.g.prefix = prefix;
	this.containerId = containerId;
	this.eventlogPrefix = '';
	this.guiFunction = null;
	this.eventGetPrice = null;
	
	/**
	* adds the gui element to the given 'e' element
	**/
	this.addGui = function(e) {
		if(e==null)
			e = document.getElementById(this.containerId);
		if(e==null){
		console.log(this.containerId+' not found or :'+e);
		return;
		}
		var elemDiv = document.createElement('div');
		elemDiv.id= this.prefix +'Quotation_gui';
		e.appendChild(elemDiv);
		if(this.guiFunction==null)
			elemDiv.innerHTML = this.createGui(this.g);
		else elemDiv.innerHTML = this.guiFunction(this.g);
		
		var e = document.getElementById(this.prefix+'Quotation_address');
		if(e!=null)
			e.value = this.c.contractAddress;
		else 
			console.log(this.prefix+'Quotation_address not found');
		this.c.bindGui();
	}	
	
	/**
	* clears the given element 'e'.
	**/
	this.clearGui = function(e){
		if(e==null)
			e = document.getElementById(this.containerId);
		e.innerHTML ='';
	}

	/**
	* Create the gui with the given 'guiFactory'. Places an sceleton arount it.
	* @return the gui txt used as innerHTML
	**/
	this.createGui = function(guifactory){
		var txt ='';
//Start of user code Quotation_create_gui_js
		txt = txt + guifactory.createDefaultGui();
//End of user code
		return guifactory.createSeletonGui(txt);

	}

	/**
	* Create a small gui, only the attributes. Places an sceleton arount it.
	**/	
	this.createSmallGui = function(guifactory){
		var txt ='';
		txt = txt + guifactory.createAttributesGui();
		return guifactory.createSeletonGui(txt);

	}

	/**
	* Update the attributes.
	**/
	this.updateGui = function(){
		this.c._updateAttributes();
	}

	/**
	* Getter for the contract 'Quotation' instance.
	**/
	this.getContract = function(){
		return this.c.instance;
	}

	/**
	* Watch for the contract events.
	* The events are stored in an element with the id this.eventlogPrefix+'eventLog'.
	**/
	this.watchEvents=function(){
	var event_GetPrice = this.getContract().GetPrice({},{fromBlock: 0});
	var elp = this.eventlogPrefix;
	var callback = this.eventGetPrice;
	event_GetPrice.watch(function(error,result){
	if(!error){
		if(callback!=null)
			callback(result);

		}else
			console.log(error);	
	});
	}

}// end of manager

/**
* Manages a list of QuotationManager uses the guid id to place the gui which is also the eventlogPrefix
*/
function QuotationGuiMananger(guiId){
	this.prefix = guiId;
	this.managers=new Array();	//[];		
	this.guiFunction = null;
	this.eventGetPrice = null;
	this.managerMap = {};
	
	/**
	* Add a contract to this manager.
	* @contract the web3 contract instance
	*/
	this.addManager = function(contract) {
		var m = new QuotationManager(contract.address,contract,this.prefix);
		m.eventlogPrefix = this.prefix;
		m.eventGetPrice = this.eventGetPrice;
		m.watchEvents();
		if(this.guiFunction!=null)
			m.guiFunction = this.guiFunction;
		this.managers.push(m);
		this.managerMap[contract.address] = m;
	}

	/**
	 * Changes the gui function for all managers.
	 */
	this.changeGuiFunction =  function(guiFunction){
		for (i in this.managers) {
			var manager = this.managers[i] ;
			manager.guiFunction = guiFunction;
		}
	}
	
	/**
	* Clears the element or the element with id 'prefix'.
	* @e an element
	*/
	this.clearGui = function(e){
		if(e==null)
			e = document.getElementById(this.prefix);
		if(e!==undefined)
			e.innerHTML ='';
	}
		
	/**
	* Displays all guis of the managed contracts.
	* @e an element
	*/
	this.displayGui = function(e){
		if(e==null)
			e = document.getElementById(this.prefix);
		if(e==undefined) return;
		for (i in this.managers) {
			var manager = this.managers[i] ;
			var elemDiv = document.createElement('div');
			elemDiv.id= manager.prefix + 'GuiContainer';//'Quotation_gui';
			e.appendChild(elemDiv);
			if(this.guiFunction==null)
				elemDiv.innerHTML = manager.createGui(manager.g);
			else elemDiv.innerHTML = this.guiFunction(manager.g);
			manager.c.bindGui();
		}
	}
	/**
	* Displays a simple gui.
	*/
	this.displaySimpleGui = function(){
		for (i in this.managers) {
			var manager = this.managers[i] ;
			manager.addGui(null);
		}
	}
	/**
	* Update the gui.
	*/
	this.updateGui = function(){
		for (i in this.managers) {
			this.managers[i].updateGui();
		}
//		console.log('update');
	}
}// end of gui mananger

/**
* Deploys the contract.
* Each constructor is available.
**/
function QuotationDeployment(guiId){
	this.prefix = guiId;
//Start of user code Quotation_deployment_attributes_js
//TODO: implement
//End of user code

	
	/**
	* The default deployer function.
	**/
	this.deployDefault = function(){
		//Start of user code Quotation_deployDefault
		//TODO: implement
		//End of user code
	}

//Start of user code Quotation_deployment_js
//TODO: implement
//End of user code
}
/**
* A simple bean class around the contract.
* The ShippingMethodModel.
**/
function ShippingMethodModel(contract) {
this.contract = contract;
	/**
	* Getter for warehouse.
	**/
	this.getWarehouse = function(){
		return contract.warehouse(); 
	}
}// end of function ShippingMethodModel

/**
* Gui factory ShippingMethod
**/
function ShippingMethodGuiFactory() {
	this.prefix='';
	
	/**
	* Places the default gui to 'e' or an element with id='ShippingMethod_gui'
	*/
	this.placeDefaultGui=function(e) {
		if(e==null)
			e = document.getElementById(this.prefix+'ShippingMethod_gui');
		if(e!=null)
			e.innerHTML = this.createDefaultGui();
		else
			console.log(this.prefix+'ShippingMethod_gui not found');
	}

	/**
	* The default generated gui with all actions and attributes.
	*/
	this.createDefaultGui=function() {
		return 		'<!-- gui for ShippingMethod_contract -->'
+		'		<div class="contract" id="'+this.prefix+'ShippingMethod_contract">'
+		'		ShippingMethod:'
+		'		  <input type="text" id="'+this.prefix+'ShippingMethod_address"> <button id="'+this.prefix+'ShippingMethodController.setAddress" onclick="'+this.prefix+'ShippingMethodController.setAddress()">change ShippingMethod Address</button>'
+		'		  <div class="contract_attributes" id="'+this.prefix+'ShippingMethod_contract_attributes"> attributes:'
+		'		    <div class="contract_attribute" id="'+this.prefix+'ShippingMethod_contract_attribute_warehouse"> warehouse:'
+		'		      <span class="contract_attribute_value" id="'+this.prefix+'ShippingMethod_warehouse_value"> </span>'
+		'		    </div>'
+		'		'
+		'		    <button class="function_btn" id="'+this.prefix+'ShippingMethod_updateAttributes" onclick="'+this.prefix+'ShippingMethodController._updateAttributes()">update ShippingMethod attributes</button>'
+		'		  </div>'
+		'		'
+		'		</div>'
;
	}

	/**
	* Create the gui for the attributes.
	*/
	this.createAttributesGui=function() {
		return 		'    <div class="contract_attribute" id="'+this.prefix+'ShippingMethod_contract_attribute_warehouse"> warehouse:'
+		'		      <span class="contract_attribute_value" id="'+this.prefix+'ShippingMethod_warehouse_value"> </span>'
+		'		    </div>'
+		'		'
;
	}

	/**
	* Create the gui.
	*/
	this.createPlainGui=function(){
		return this.createAttributesGui()
				;
	}

	/**
	* Create a div with '@inner' as inner elements.
    * @inner - the inner text
	*/
	this.createSeletonGui=function(inner) {
		return 	'<!-- gui for ShippingMethod_contract -->'
		+	'	<div class="contract" id="'+this.prefix+'ShippingMethod_contract">'
		+ inner
		+'</div>';
	}


	//eventguis

}//end guifactory

/**
* Class ShippingMethodController. 
* The controller wrap's the 'instance' contract and binds all actions to document elements.
* Parameters are taken from elements with self.prefix+'functionName_parameterName'
*
* self.prefix+'ShippingMethodController.setAddress' - 
* self.prefix+'ShippingMethod_updateAttributes'     - 
*/
function ShippingMethodController() {

	this.instance = undefined;
	this.prefix='';
	this.contractAddress = undefined; 
	this.eventlogPrefix = '';
	var self = this;

	/**
	* Binds the element with the prefix-id to the corresponding element.
	*/
	this.bindGui=function() {
		console.log('bind gui for:'+self.prefix);
		var btn = document.getElementById(self.prefix+'ShippingMethodController.setAddress');
		if(btn!=undefined)		
			btn.onclick = this.setAddress;
		else console.log('addres widget not bound');

		var btn = document.getElementById(self.prefix+'ShippingMethod_updateAttributes');
		if(btn!=undefined)
			btn.onclick = this._updateAttributes;
		else console.log('_updateAttributes widget not bound');

	}

	/**
	* Set the address.
	*/
	this.setAddress=function() {
		var _address = document.getElementById(self.prefix+'ShippingMethod_address');
		if(_address==null)return;

		self.ShippingMethod_instance = ShippingMethodContract.at(_address.value);
		self.contractAddress = _address.value;
		self._updateAttributes();
	}
	
	/**
	* Update attributes.
	*
	* prefix+'ShippingMethod_warehouse_value' - 
	**/
	this._updateAttributes=function () {
		if(this.instance===null) return;
		// update attributes
		var warehouse_res = self.instance.warehouse();
		var e = document.getElementById(self.prefix+'ShippingMethod_warehouse_value');
		if(warehouse_res!=null && e!=null)
			e.innerText = warehouse_res;
		else console.log(self.prefix+'ShippingMethod_warehouse_value not found');
	}

	//call functions
	
//delegated calls
}// end controller	

/**
* class as GlueCode ShippingMethodManager,
* uses prefix + 'GuiContainer' as target element.
* It combines a ShippingMethodController as 'c' and a ShippingMethodGuiFactory as 'g'.
**/
function ShippingMethodManager(prefix,contract,containerId) {
	this.prefix = prefix;
	var self = this;
	this.c = new ShippingMethodController();
	this.c.prefix=prefix;
	this.c.instance=contract;
	this.c.contractAddress = contract.address;
	this.g = new ShippingMethodGuiFactory();
	this.g.prefix = prefix;
	this.containerId = containerId;
	this.eventlogPrefix = '';
	this.guiFunction = null;
	
	/**
	* adds the gui element to the given 'e' element
	**/
	this.addGui = function(e) {
		if(e==null)
			e = document.getElementById(this.containerId);
		if(e==null){
		console.log(this.containerId+' not found or :'+e);
		return;
		}
		var elemDiv = document.createElement('div');
		elemDiv.id= this.prefix +'ShippingMethod_gui';
		e.appendChild(elemDiv);
		if(this.guiFunction==null)
			elemDiv.innerHTML = this.createGui(this.g);
		else elemDiv.innerHTML = this.guiFunction(this.g);
		
		var e = document.getElementById(this.prefix+'ShippingMethod_address');
		if(e!=null)
			e.value = this.c.contractAddress;
		else 
			console.log(this.prefix+'ShippingMethod_address not found');
		this.c.bindGui();
	}	
	
	/**
	* clears the given element 'e'.
	**/
	this.clearGui = function(e){
		if(e==null)
			e = document.getElementById(this.containerId);
		e.innerHTML ='';
	}

	/**
	* Create the gui with the given 'guiFactory'. Places an sceleton arount it.
	* @return the gui txt used as innerHTML
	**/
	this.createGui = function(guifactory){
		var txt ='';
//Start of user code ShippingMethod_create_gui_js
		txt = txt + guifactory.createDefaultGui();
//End of user code
		return guifactory.createSeletonGui(txt);

	}

	/**
	* Create a small gui, only the attributes. Places an sceleton arount it.
	**/	
	this.createSmallGui = function(guifactory){
		var txt ='';
		txt = txt + guifactory.createAttributesGui();
		return guifactory.createSeletonGui(txt);

	}

	/**
	* Update the attributes.
	**/
	this.updateGui = function(){
		this.c._updateAttributes();
	}

	/**
	* Getter for the contract 'ShippingMethod' instance.
	**/
	this.getContract = function(){
		return this.c.instance;
	}

	/**
	* Watch for the contract events.
	* The events are stored in an element with the id this.eventlogPrefix+'eventLog'.
	**/
	this.watchEvents=function(){
	}

}// end of manager

/**
* Manages a list of ShippingMethodManager uses the guid id to place the gui which is also the eventlogPrefix
*/
function ShippingMethodGuiMananger(guiId){
	this.prefix = guiId;
	this.managers=new Array();	//[];		
	this.guiFunction = null;
	this.managerMap = {};
	
	/**
	* Add a contract to this manager.
	* @contract the web3 contract instance
	*/
	this.addManager = function(contract) {
		var m = new ShippingMethodManager(contract.address,contract,this.prefix);
		m.eventlogPrefix = this.prefix;
		m.watchEvents();
		if(this.guiFunction!=null)
			m.guiFunction = this.guiFunction;
		this.managers.push(m);
		this.managerMap[contract.address] = m;
	}

	/**
	 * Changes the gui function for all managers.
	 */
	this.changeGuiFunction =  function(guiFunction){
		for (i in this.managers) {
			var manager = this.managers[i] ;
			manager.guiFunction = guiFunction;
		}
	}
	
	/**
	* Clears the element or the element with id 'prefix'.
	* @e an element
	*/
	this.clearGui = function(e){
		if(e==null)
			e = document.getElementById(this.prefix);
		if(e!==undefined)
			e.innerHTML ='';
	}
		
	/**
	* Displays all guis of the managed contracts.
	* @e an element
	*/
	this.displayGui = function(e){
		if(e==null)
			e = document.getElementById(this.prefix);
		if(e==undefined) return;
		for (i in this.managers) {
			var manager = this.managers[i] ;
			var elemDiv = document.createElement('div');
			elemDiv.id= manager.prefix + 'GuiContainer';//'ShippingMethod_gui';
			e.appendChild(elemDiv);
			if(this.guiFunction==null)
				elemDiv.innerHTML = manager.createGui(manager.g);
			else elemDiv.innerHTML = this.guiFunction(manager.g);
			manager.c.bindGui();
		}
	}
	/**
	* Displays a simple gui.
	*/
	this.displaySimpleGui = function(){
		for (i in this.managers) {
			var manager = this.managers[i] ;
			manager.addGui(null);
		}
	}
	/**
	* Update the gui.
	*/
	this.updateGui = function(){
		for (i in this.managers) {
			this.managers[i].updateGui();
		}
//		console.log('update');
	}
}// end of gui mananger

/**
* Deploys the contract.
* Each constructor is available.
**/
function ShippingMethodDeployment(guiId){
	this.prefix = guiId;
//Start of user code ShippingMethod_deployment_attributes_js
//TODO: implement
//End of user code

	
	/**
	* The default deployer function.
	**/
	this.deployDefault = function(){
		//Start of user code ShippingMethod_deployDefault
		//TODO: implement
		//End of user code
	}

//Start of user code ShippingMethod_deployment_js
//TODO: implement
//End of user code
}
/**
* A simple bean class around the contract.
* The ContractModel.
**/
function ContractModel(contract) {
this.contract = contract;
	/**
	* Getter for purchaseorder.
	**/
	this.getPurchaseorder = function(){
		return contract.purchaseorder(); 
	}
}// end of function ContractModel

/**
* Gui factory Contract
**/
function ContractGuiFactory() {
	this.prefix='';
	
	/**
	* Places the default gui to 'e' or an element with id='Contract_gui'
	*/
	this.placeDefaultGui=function(e) {
		if(e==null)
			e = document.getElementById(this.prefix+'Contract_gui');
		if(e!=null)
			e.innerHTML = this.createDefaultGui();
		else
			console.log(this.prefix+'Contract_gui not found');
	}

	/**
	* The default generated gui with all actions and attributes.
	*/
	this.createDefaultGui=function() {
		return 		'<!-- gui for Contract_contract -->'
+		'		<div class="contract" id="'+this.prefix+'Contract_contract">'
+		'		Contract:'
+		'		  <input type="text" id="'+this.prefix+'Contract_address"> <button id="'+this.prefix+'ContractController.setAddress" onclick="'+this.prefix+'ContractController.setAddress()">change Contract Address</button>'
+		'		  <div class="contract_attributes" id="'+this.prefix+'Contract_contract_attributes"> attributes:'
+		'		    <div class="contract_attribute" id="'+this.prefix+'Contract_contract_attribute_purchaseorder"> purchaseorder:'
+		'		      <span class="contract_attribute_value" id="'+this.prefix+'Contract_purchaseorder_value"> </span>'
+		'		    </div>'
+		'		'
+		'		    <button class="function_btn" id="'+this.prefix+'Contract_updateAttributes" onclick="'+this.prefix+'ContractController._updateAttributes()">update Contract attributes</button>'
+		'		  </div>'
+		'		'
+		'		</div>'
;
	}

	/**
	* Create the gui for the attributes.
	*/
	this.createAttributesGui=function() {
		return 		'    <div class="contract_attribute" id="'+this.prefix+'Contract_contract_attribute_purchaseorder"> purchaseorder:'
+		'		      <span class="contract_attribute_value" id="'+this.prefix+'Contract_purchaseorder_value"> </span>'
+		'		    </div>'
+		'		'
;
	}

	/**
	* Create the gui.
	*/
	this.createPlainGui=function(){
		return this.createAttributesGui()
				;
	}

	/**
	* Create a div with '@inner' as inner elements.
    * @inner - the inner text
	*/
	this.createSeletonGui=function(inner) {
		return 	'<!-- gui for Contract_contract -->'
		+	'	<div class="contract" id="'+this.prefix+'Contract_contract">'
		+ inner
		+'</div>';
	}


	//eventguis

}//end guifactory

/**
* Class ContractController. 
* The controller wrap's the 'instance' contract and binds all actions to document elements.
* Parameters are taken from elements with self.prefix+'functionName_parameterName'
*
* self.prefix+'ContractController.setAddress' - 
* self.prefix+'Contract_updateAttributes'     - 
*/
function ContractController() {

	this.instance = undefined;
	this.prefix='';
	this.contractAddress = undefined; 
	this.eventlogPrefix = '';
	var self = this;

	/**
	* Binds the element with the prefix-id to the corresponding element.
	*/
	this.bindGui=function() {
		console.log('bind gui for:'+self.prefix);
		var btn = document.getElementById(self.prefix+'ContractController.setAddress');
		if(btn!=undefined)		
			btn.onclick = this.setAddress;
		else console.log('addres widget not bound');

		var btn = document.getElementById(self.prefix+'Contract_updateAttributes');
		if(btn!=undefined)
			btn.onclick = this._updateAttributes;
		else console.log('_updateAttributes widget not bound');

	}

	/**
	* Set the address.
	*/
	this.setAddress=function() {
		var _address = document.getElementById(self.prefix+'Contract_address');
		if(_address==null)return;

		self.Contract_instance = ContractContract.at(_address.value);
		self.contractAddress = _address.value;
		self._updateAttributes();
	}
	
	/**
	* Update attributes.
	*
	* prefix+'Contract_purchaseorder_value' - 
	**/
	this._updateAttributes=function () {
		if(this.instance===null) return;
		// update attributes
		var purchaseorder_res = self.instance.purchaseorder();
		var e = document.getElementById(self.prefix+'Contract_purchaseorder_value');
		if(purchaseorder_res!=null && e!=null)
			e.innerText = purchaseorder_res;
		else console.log(self.prefix+'Contract_purchaseorder_value not found');
	}

	//call functions
	
//delegated calls
}// end controller	

/**
* class as GlueCode ContractManager,
* uses prefix + 'GuiContainer' as target element.
* It combines a ContractController as 'c' and a ContractGuiFactory as 'g'.
**/
function ContractManager(prefix,contract,containerId) {
	this.prefix = prefix;
	var self = this;
	this.c = new ContractController();
	this.c.prefix=prefix;
	this.c.instance=contract;
	this.c.contractAddress = contract.address;
	this.g = new ContractGuiFactory();
	this.g.prefix = prefix;
	this.containerId = containerId;
	this.eventlogPrefix = '';
	this.guiFunction = null;
	
	/**
	* adds the gui element to the given 'e' element
	**/
	this.addGui = function(e) {
		if(e==null)
			e = document.getElementById(this.containerId);
		if(e==null){
		console.log(this.containerId+' not found or :'+e);
		return;
		}
		var elemDiv = document.createElement('div');
		elemDiv.id= this.prefix +'Contract_gui';
		e.appendChild(elemDiv);
		if(this.guiFunction==null)
			elemDiv.innerHTML = this.createGui(this.g);
		else elemDiv.innerHTML = this.guiFunction(this.g);
		
		var e = document.getElementById(this.prefix+'Contract_address');
		if(e!=null)
			e.value = this.c.contractAddress;
		else 
			console.log(this.prefix+'Contract_address not found');
		this.c.bindGui();
	}	
	
	/**
	* clears the given element 'e'.
	**/
	this.clearGui = function(e){
		if(e==null)
			e = document.getElementById(this.containerId);
		e.innerHTML ='';
	}

	/**
	* Create the gui with the given 'guiFactory'. Places an sceleton arount it.
	* @return the gui txt used as innerHTML
	**/
	this.createGui = function(guifactory){
		var txt ='';
//Start of user code Contract_create_gui_js
		txt = txt + guifactory.createDefaultGui();
//End of user code
		return guifactory.createSeletonGui(txt);

	}

	/**
	* Create a small gui, only the attributes. Places an sceleton arount it.
	**/	
	this.createSmallGui = function(guifactory){
		var txt ='';
		txt = txt + guifactory.createAttributesGui();
		return guifactory.createSeletonGui(txt);

	}

	/**
	* Update the attributes.
	**/
	this.updateGui = function(){
		this.c._updateAttributes();
	}

	/**
	* Getter for the contract 'Contract' instance.
	**/
	this.getContract = function(){
		return this.c.instance;
	}

	/**
	* Watch for the contract events.
	* The events are stored in an element with the id this.eventlogPrefix+'eventLog'.
	**/
	this.watchEvents=function(){
	}

}// end of manager

/**
* Manages a list of ContractManager uses the guid id to place the gui which is also the eventlogPrefix
*/
function ContractGuiMananger(guiId){
	this.prefix = guiId;
	this.managers=new Array();	//[];		
	this.guiFunction = null;
	this.managerMap = {};
	
	/**
	* Add a contract to this manager.
	* @contract the web3 contract instance
	*/
	this.addManager = function(contract) {
		var m = new ContractManager(contract.address,contract,this.prefix);
		m.eventlogPrefix = this.prefix;
		m.watchEvents();
		if(this.guiFunction!=null)
			m.guiFunction = this.guiFunction;
		this.managers.push(m);
		this.managerMap[contract.address] = m;
	}

	/**
	 * Changes the gui function for all managers.
	 */
	this.changeGuiFunction =  function(guiFunction){
		for (i in this.managers) {
			var manager = this.managers[i] ;
			manager.guiFunction = guiFunction;
		}
	}
	
	/**
	* Clears the element or the element with id 'prefix'.
	* @e an element
	*/
	this.clearGui = function(e){
		if(e==null)
			e = document.getElementById(this.prefix);
		if(e!==undefined)
			e.innerHTML ='';
	}
		
	/**
	* Displays all guis of the managed contracts.
	* @e an element
	*/
	this.displayGui = function(e){
		if(e==null)
			e = document.getElementById(this.prefix);
		if(e==undefined) return;
		for (i in this.managers) {
			var manager = this.managers[i] ;
			var elemDiv = document.createElement('div');
			elemDiv.id= manager.prefix + 'GuiContainer';//'Contract_gui';
			e.appendChild(elemDiv);
			if(this.guiFunction==null)
				elemDiv.innerHTML = manager.createGui(manager.g);
			else elemDiv.innerHTML = this.guiFunction(manager.g);
			manager.c.bindGui();
		}
	}
	/**
	* Displays a simple gui.
	*/
	this.displaySimpleGui = function(){
		for (i in this.managers) {
			var manager = this.managers[i] ;
			manager.addGui(null);
		}
	}
	/**
	* Update the gui.
	*/
	this.updateGui = function(){
		for (i in this.managers) {
			this.managers[i].updateGui();
		}
//		console.log('update');
	}
}// end of gui mananger

/**
* Deploys the contract.
* Each constructor is available.
**/
function ContractDeployment(guiId){
	this.prefix = guiId;
//Start of user code Contract_deployment_attributes_js
//TODO: implement
//End of user code

	
	/**
	* The default deployer function.
	**/
	this.deployDefault = function(){
		//Start of user code Contract_deployDefault
		//TODO: implement
		//End of user code
	}

//Start of user code Contract_deployment_js
//TODO: implement
//End of user code
}

/**
* A class to manage a single page dapp.
* The SupplyChainDemoPage object uses the managers to display the gui.
**/
function SupplyChainDemoPage(prefix) {
	this.prefix=prefix;
	//Start of user code page_SupplyChainDemo_attributes
		//TODO: implement
	//End of user code

	
	/**
	* Places the default gui in the page.
	**/
	this.placeDefaultGui=function() {
	this.createDefaultGui();
	}
/**
* Create the default Gui.
* Use this method to customize the gui.
**/
this.createDefaultGui=function() {
	//Start of user code page_SupplyChainDemo_create_default_gui_functions
		//TODO: implement
	//End of user code
}
	//Start of user code page_SupplyChainDemo_functions
		//TODO: implement
	//End of user code

}// end of SupplyChainDemoPage

//Start of user code SupplyChainDemo_custom_functions
		//TODO: implement
//End of user code
